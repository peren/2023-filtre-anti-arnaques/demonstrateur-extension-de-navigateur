#!/usr/bin/python

# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import hashlib
import json
import sys
import urllib.request
from urllib.parse import urlparse

BLOCKED_SITES_URL = "http://127.0.0.0:8080/clear.json"
ADBLOCK_FORMAT = "adblock_format.txt"
SAFEBROWSING_FORMAT = "safebrowsing_format.txt"
HOSTS_FORMAT = "hosts_format.txt"


def is_not_specific_ressource(url):
    """
    test if a url is not a specific ressource
    if it is, it does not justify blocking the whole domain
    """
    if urlparse("https://" + url).path in {"", "/"}:
        return True
    return False


def get_signaled_infos():
    with urllib.request.urlopen(BLOCKED_SITES_URL) as url:
        data = json.load(url)
    return data


def write_list(filepath, blocked_list):
    with open(filepath, "w", encoding="utf-8") as file_handler:
        file_handler.writelines(blocked_list)


def main():
    data = get_signaled_infos()
    write_list(
        filepath=ADBLOCK_FORMAT,
        blocked_list=[
            f"! signaleur {v[0]}, motif {v[1]}, contact {v[2]}\n||{url}^\n"
            for url, v in data.items()
            if is_not_specific_ressource(url)
        ],
    )
    write_list(
        filepath=SAFEBROWSING_FORMAT,
        blocked_list=[
            f"{hashlib.sha256(bytes(url, encoding='utf-8')).hexdigest()}\n"
            for url in data
        ],
    )
    write_list(
        filepath=HOSTS_FORMAT,
        blocked_list=[
            f"# signaleur {v[0]}, motif {v[1]}, contact {v[2]}\n0.0.0.0 {url}\n"
            for url, v in data.items()
            if is_not_specific_ressource(url)
        ],
    )
    return 0


if __name__ == "__main__":
    sys.exit(main())
