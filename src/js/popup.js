// SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
//
// SPDX-License-Identifier: MIT

import { dom } from './dom.js';

var checked = await getFiltering();

if (checked) { dom.attr('#optOut', "checked", "checked"); }
else { dom.attr('#optOut', "checked", null); }

const optOut = async function () {
    await browser.runtime.sendMessage({
        what: 'optOut',
    });
};

const optIn = async function () {
    await browser.runtime.sendMessage({
        what: 'refreshBlacklist',
    });
};

const resetWhitelist = async function (checked) {
    await browser.runtime.sendMessage({
        what: 'resetWhitelist',
        checked: checked,
    });
};

dom.on('#optOut', 'change', ev => {
    checked = ev.target.checked;
    if (checked) {
        optIn();
    }
    else {
        optOut();
    }
});

dom.on('#resetWhitelist', 'click', () => {
    resetWhitelist(checked);
});