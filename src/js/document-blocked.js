// SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
//
// SPDX-License-Identifier: MIT

import { dom } from './dom.js';

let details = {};

{
    const matches = /details=([^&]+)/.exec(window.location.search);
    if (matches !== null) {
        details = JSON.parse(decodeURIComponent(matches[1]));
    }
}

dom.text('#theurl', details.spottedUrl); 
dom.text('#therisk', details.risk); 
dom.text('#theauthority', details.authority); 
dom.text('#themail', details.email);

const getBlockedResource = function() {
    return details.spottedUrl;
};

const proceedToResource = function () {
    window.location.replace(details.url);
};

const proceedPermanent = async function () {
    await browser.runtime.sendMessage({
        what: 'refreshBlacklist',
        hostname: getBlockedResource(),
    });
    proceedToResource();
};


dom.on('#proceed', 'click', () => {
    proceedPermanent();
});
