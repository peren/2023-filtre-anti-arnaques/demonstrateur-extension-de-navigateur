// SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
//
// SPDX-License-Identifier: MIT

const blockedUrl = browser.runtime.getURL('src/document_blocked.html');
var resourceList = [];
var reasons = {};

function redirect(requestDetails) {
  // based on the SafeBrowsing methodology https://developers.google.com/safe-browsing/v4/urls-hashing
  const reMatch = [...requestDetails.url.matchAll(/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)([^#\n?]+)?(\?[^#\n]+)?/ig)][0];
  /*
  The expression has three non-capturing groups and three capturing groups:
  - Non-capturing: `(?:https?:\/\/)?` captures `http://` or `https://` if present
  - Non-capturing: `(?:[^@\n]+@)?` captures anything that might be present before `www.`
  - Non-capturing: `(?:www\.)?` captures `www.` if present
  - Capturing: `([^#:\/\n?]+)` captures the domain, including subdomains, until the beginning of the path
  - Capturing: `([^#\n?]+)?` captures the path if it exists
  - Capturing: `(\?[^#\n]+)?` captures the query if it exists
  */
  const rawDomain = reMatch[1] ?? "";
  const rawPath = reMatch[2] ?? "";
  const rawQuery = reMatch[3] ?? "";
  const cleanDomain = rawDomain.replace(/^\.+|\.+$/g, "").replace(/\.+/g, ".").toLowerCase();
  const domainLength = cleanDomain.split(".").length;
  // Remove leading and trailing dots, replace consecutive dots with a single dot, lowercase
  // TODO: Detect and parse legal IP encodings, including in octal/hex and fewer than four components
  // -> Note browsers tend to parse IP addresses automatically 
  const cleanPath = rawPath.replace(/\/+/g, "/");
  const pathLength = cleanPath.split("/").length;
  // Replace consecutive slashes with a single slash
  let cutDomain, cutPath, cutQuery;
  for (let domainIndex = 0; domainIndex < Math.min(5, domainLength - 1); domainIndex++) {
    for (let pathIndex = 0; pathIndex < Math.min(6, pathLength + 1); pathIndex++) {
      if (domainIndex === 0) { cutDomain = cleanDomain; }
      else { cutDomain = cleanDomain.split(".").slice(- domainIndex - 1).join("."); }
      cutPath = cleanPath.split("/").slice(0, pathLength - (pathIndex - 1)).join("/");
      cutQuery = "/";
      if (pathIndex === 0) { cutQuery = rawQuery; }
      else if (pathIndex === 1) { cutQuery = ""; }
      const rawURL = cutDomain + cutPath + cutQuery;
      const hashedURL = sha256(rawURL);
      if (resourceList.includes(hashedURL)) {
        const [authority, risk, email] = reasons[hashedURL];
        return {
          redirectUrl: blockedUrl + `?details={"url":"${requestDetails.url}","spottedUrl":"${rawURL}","risk":"${risk}","authority":"${authority}","email":"${email}"}`
        };
      }
    }
  }
  return;
}

function difference(setA, setB) {
  const _difference = new Set(setA);
  for (const elem of setB) {
    _difference.delete(elem);
  }
  return _difference;
}

async function updateBlacklist() {
  // Replace the following URL with any address serving a list
  const fraudulentBaseURL = "http://127.0.0.0:8080/hashed.json";
  const fraudulentHashes = await (await fetch(fraudulentBaseURL)).json();
  return setBlacklist(fraudulentHashes);
}

async function refreshBlacklist() {
  await setFiltering(true);
  const blacklist = new Set(await getBlacklist());
  const whitelist = new Set(await getWhitelist());
  resourceList = [...difference(blacklist, whitelist)];
  reasons = await getReasons();;
  console.log(`Current blacklist: ${resourceList}`);
  browser.webRequest.onBeforeRequest.addListener(
    redirect,
    { urls: ["*://*/*"] },
    ["blocking"]
  );
}

updateBlacklist().then(async () => {
  refreshBlacklist();
})

browser.runtime.onMessage.addListener(
  async function (request, sender, sendResponse) {
    console.log(`Message: ${request.what}`)
    if (request.what === "refreshBlacklist") {
      browser.webRequest.onBeforeRequest.removeListener(redirect);
      if (request.hostname) {
        await addToWhitelist(sha256(request.hostname));
      }
      await refreshBlacklist();
      return;
    }
    else if (request.what === "optOut") {
      browser.webRequest.onBeforeRequest.removeListener(redirect);
      await setFiltering(false);
      return;
    }
    else if (request.what === "resetWhitelist") {
      await setWhitelist([]);
      if (request.checked) {
        await refreshBlacklist();
      }
      return;
    }
    else {
      console.log("Unknown message sent to background");
      return;
    }
  }
)
