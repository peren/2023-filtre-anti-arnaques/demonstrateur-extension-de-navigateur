// SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
//
// SPDX-License-Identifier: MIT

async function getFiltering() {
  return browser.storage.local.get("filtering").then(result => result.filtering);
}

async function setFiltering(filtering) {
  return browser.storage.local.set({ filtering: filtering });
}

async function getBlacklist() {
  return browser.storage.local.get("blacklist").then(result => result.blacklist);
}

async function setBlacklist(blacklistJson) {
  const blacklistObjects = Object.keys(blacklistJson);
  browser.storage.local.set({ reasons: blacklistJson });
  return browser.storage.local.set({ blacklist: [...new Set(blacklistObjects)] });
}

async function getWhitelist() {
  return browser.storage.local.get("whitelist").then(result => result.whitelist);
}

async function setWhitelist(whitelistObjects) {
  return browser.storage.local.set({ whitelist: [...new Set(whitelistObjects)] });
}

async function getReasons() {
  return browser.storage.local.get("reasons").then(result => result.reasons);
}

async function addToWhitelist(whitelist_elt) {
  return getWhitelist().then(oldWhitelist => { if (oldWhitelist) setWhitelist([...oldWhitelist, whitelist_elt]); else setWhitelist([whitelist_elt]); });
}
