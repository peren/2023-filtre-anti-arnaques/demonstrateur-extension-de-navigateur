# EXtension ANTi Escroquerie - EXANTE

L'extension de navigateur EXANTE est un démonstrateur des fonctionnalités et de l'expérience utilisateur d'un "filtre anti-arnaque".

## Principe de fonctionnement

Le fonctionnement de l'extension s'inspire des bloqueurs de publicités et des services tels que Google SafeBrowsing ou Microsoft Smartscreen pour la gestion des URLs des ressources à bloquer.

Le navigateur télécharge une liste de ressources lors de son lancement puis bloque l'intégralité des requêtes quand il essaie d'accéder à l'une de ces ressources. L'utilisateur fait alors face à une page l'avertissant du blocage de la ressource, et peut décider d'accepter le risque et poursuivre en ajoutant cette ressource à sa liste blanche. L'utilisateur peut également désactiver complètement le filtre d'un clic ou effacer sa liste blanche.

Aucune information ne remonte à l'extension en dehors du navigateur.

## Tester l'extension

1. Lancer un serveur depuis la racine du dépôt pour rendre disponible à l'extension la liste de sites marqués : `$ python3 -m http.server -d static/ -b 127.0.0.0 8080` ;
1. Ouvrir votre navigateur et charger l'extension :
    - [sur Chrome](https://developer.chrome.com/docs/extensions/mv3/getstarted/development-basics/#load-unpacked),
    - [sur Firefox](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing).
2. Consulter les exemples :
    - de [site dangereux](https://www.example.com/),
    - de [ressource dangereuse](https://fr.wiktionary.org/wiki/exemple).

## Partage de listes sous différents formats

Le script de `write_lists` illustre la notion de commun numérique qu'il est possible de construire à partir d'une base de ressources à risques en la traduisant sous différents formats (safebrowsing, bloqueur de publicité, ...). Avec le précédent serveur toujours actif, lancer dans un autre terminal : `$ python3 -m write_lists`.

## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte de la licence se trouve dans le dossier [`./LICENSES`](./LICENSES/MIT.txt).